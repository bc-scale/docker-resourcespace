## Docker setup of ResourceSpace, with Linnaeus plugins

[Linnaeus](https://gitlab.com/naturalis/bii/linnaeus/linnaeus_ng) uses ResourceSpace (RS; [www.resourcespace.com](https://www.resourcespace.com))
for centralized storage of media.
The link between both applications was once established using
plugins for the RS API. Unfortunately, the option to use these API plugins was
unceremoniously dumped in the RS v7.9 update. 

In order to replicate the lost api plugin functionality,
part of the dropped RS code was incorporated into plugins.
Access to the database is provided by setting environment variables, which set
essential parameters in the config.php of the plugin.

This docker image and docker-compose setup was created to install a recent version of RS,
combined with the missing Linnaeus plugins.

### Prepare local development environment

1. Clone this repository
2. Create a directory in the root of the project `mkdir -p data/database/init`
3. If the directory is already there, be sure to throw away `data/database/storage`
4. Create and retrieve a recent backup dump of the RS MySQL database from a running resourcespace instance: `docker-compose exec database /bin/sh -c 'mysqldump -q --routines -q --user=root --password=$MYSQL_ROOT_PASSWORD resourcespace'  > ~development/db.sql`.
5. Place the recent backup dump in `data/database/init`
6. Stop all other webservers and docker systems on your local system
7. Copy env.template to a `.env` file lookup SPIDER_PASSWORD, SCRAMBLE_KEY and API_SCRAMBLE_KEY and the AWS secrets for dryrun link in Bitwarden
8. You can also override `IMAGE_VERSION` to the image build of the branch if you do not want to use the default `latest`
9. Download the most recent version of the docker images `docker-compose pull`
10. Start the environment `docker-compose up`, wait until the database is loaded
11. Add `127.0.0.1   resourcespace.dryrun.link` to your local `/etc/hosts` file
12. If the database is ready, start your favorite browser goto http://resourcespace.dryrun.link

After logging in using the admin password (which is stored in bitwarden), you should
see the dashboard.

If you see an error about a missing 'slideshow' directory, fix this by making the missing directory:

```
mkdir -p data/files/system/slideshow
sudo chown -R 82:82 data/files
```

### Linnaeus resourcespace communication

The following steps are used to set up a connection between Linnaeus and RS:

1. A dedicated RS user is generated when setting up the Media module in Linnaeus for the first time.  This requires a 'master key', essentially providing one-time access to the RS admin account through the api. This key is stored in the database and in bitwarden and **should never be shared**. The methods to create the api key have been replicated in the plugin. The plugin also provides a method to expose the api key if both user and password are known.
2. The plugin creates a new user (based on Linnaeus project and server name) and automatically generates a password. If created successfully, the api returns the user id, collection id and api key for the Linnaeus project as json. Linnaeus stores this information. The Media module is now successfully activated.
3. A Linnaeus user posts a file, also providing the Linnaeus api key. The same RS methods are used in the plugin (only now using proper OOP code :) to store the file. When successfully stored, the RS properties of the file are returned as json.
4. Linnaeus stores the relevant information (urls, image properties) locally.

### Further information

Inline documentation in the plugins provides more technical information.

 - [api_new_user_lng](linnaeus_api_plugins/controllers/UserController.php)
 - [api_upload_lng](linnaeus_api_plugins/controllers/UploadController.php)

You should also check the smoketests which give a good illustration the way the
communication between Linnaeus and resourcespace should function:

 - [smoke-test.json](qa/postman/smoke-test.json)

The smoke tests are run in the gitlab pipeline. You can run these by hand by following
the instructions in [this merge request](https://gitlab.com/naturalis/bii/resourcespace/docker-resourcespace/-/merge_requests/5).
