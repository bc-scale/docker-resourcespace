#!/bin/sh
set -eu

cd "$(dirname "$0")"

#compose_version=$(wget -qO- https://api.github.com/repos/docker/compose/releases/latest | grep 'tag_name' | cut -d\" -f4)
compose_version="1.29.2"
image_version=build_$CI_COMMIT_REF_SLUG

mkdir -p "data"
chown 82:82 "data"

docker pull "$CI_REGISTRY_IMAGE/nginx:$image_version"
docker pull "$CI_REGISTRY_IMAGE/php-fpm:$image_version"
docker pull "registry.gitlab.com/naturalis/bii/linnaeus/docker-linnaeus/docker-proxy:latest"
docker pull "registry.gitlab.com/naturalis/bii/linnaeus/docker-linnaeus/mariadb:latest"
docker run --rm -v "$PWD":"$PWD":ro -v /var/run/docker.sock:/var/run/docker.sock:ro -e "IMAGE_VERSION=$image_version" \
  docker/compose:"$compose_version" -f "$PWD/docker-compose.yml" up -d

sleep 5
docker run --rm -t -v "$PWD/postman":/etc/newman -v "$PWD/pictures":/tmp/pictures --network qa_web -e TEST postman/newman:5-alpine run smoke-test.json --insecure --env-var "admin_key=$TEST_API_KEY"
