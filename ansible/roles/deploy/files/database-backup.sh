#!/bin/bash
set -eu

project=${1-'resourcespace'}
project_path=/opt/compose_projects/${project}
backup_path=${2-/data/backup}
backup_file=${backup_path}/${project}_$(date +%Y-%m-%d_%H%M%S).sql.gz
days=${3-'1'}

if [ -z "${project}" ]; then
    echo "You need to specificy a project and optional path to backup directory"
    echo "$0 project_name [/path/to/backup/directory/]"
    exit 1
fi
if [ ! -d "$project_path" ]; then
    echo "Project directory $project_path does not exist"
    exit 1
fi
if [ ! -d "$backup_path" ]; then
    echo "Backup directory $backup_path does not exist"
    exit 1
fi
echo "Remove files older than ${days} days"
/usr/bin/find "${backup_path}" -mtime +"${days}" -type f -name 'resourcespace*.gz' -exec rm -rf {} \;
echo "$(date) - ${project} - database backup started"
# shellcheck disable=SC2016
/usr/local/bin/docker-compose \
    --env-file "${project_path}/.env" \
    --file "${project_path}/docker-compose.yml" \
    --file "${project_path}/docker-compose.override.yml" \
    run -T --rm database \
    /bin/sh -c '/usr/bin/mysqldump --routines --skip-disable-keys --host=database --user=$MYSQL_USER --password=$MYSQL_PASSWORD $MYSQL_DATABASE' | /bin/gzip > "${backup_file}"
