#!/bin/sh
set -eu

basedir=$(dirname "$0")

if [ -z ${ANSIBLE_CONFIG+x} ]; then
  echo Missing ANSIBLE_CONFIG environment variable
  exit 1;
fi

mkdir -p "$basedir/secrets"
echo "$ANSIBLE_DEPLOY_KEY" | base64 -d > "$basedir/secrets/id_ed25519"
chmod 400 "$basedir/secrets/id_ed25519"

mkdir -m 700 "$basedir/secrets/ssh"
cat "$ANSIBLE_KNOWN_HOSTS" > "$basedir/secrets/ssh/known_hosts"
chmod 644 "$basedir/secrets/ssh/known_hosts"
